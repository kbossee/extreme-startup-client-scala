package controllers

import com.typesafe.scalalogging.LazyLogging
import javax.inject._
import play.api.mvc._

@Singleton
class ClientController @Inject()(cc: ControllerComponents) extends AbstractController(cc) with LazyLogging {

  def ask(question:String) = Action { implicit request: Request[AnyContent] =>
    val a = answer(question).toString
    logger.info(s"'$question' => '$a'")
    Ok(a)
  }

  private def answer(question:String): Any = {
    ""
  }
}
